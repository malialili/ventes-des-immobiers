package fr.universite.bordeaux.repositories;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.universite.bordeaux.entities.Annonce;
import fr.universite.bordeaux.entities.User;

@Stateless
public class AnnonceRepository {
    private static final String JPQL_SELECT_PAR_EMAIL = "SELECT a FROM Annonce a WHERE a.user=:user";
    private static final String PARAM_USER = "user";
    @PersistenceContext(unitName = "aldaPersistenceUnit")
    private EntityManager entityManager;

    public void addAnnonce(Annonce annonce){
        entityManager.persist(annonce);
    }

    public List<Annonce> findAnnoncesByUser(User user) {
        Query requete = entityManager.createQuery(JPQL_SELECT_PAR_EMAIL);
        requete.setParameter(PARAM_USER, user);
        @SuppressWarnings("unchecked")
        List<Annonce> announcements = (List<Annonce>)requete.getResultList();
        return announcements;
    }
}
