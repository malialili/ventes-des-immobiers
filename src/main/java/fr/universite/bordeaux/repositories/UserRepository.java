package fr.universite.bordeaux.repositories;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.*;
//import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import fr.universite.bordeaux.entities.PaginatedListWrapper;
import fr.universite.bordeaux.entities.User;

@Stateless
@ApplicationPath("/resources")
@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserRepository {
	private static final String JPQL_SELECT_PAR_EMAIL = "SELECT u FROM User u WHERE u.email=:email";
	private static final String PARAM_EMAIL = "email";
	@PersistenceContext(unitName = "aldaPersistenceUnit")
	private EntityManager entityManager;

	public void addUser(User user){
		entityManager.persist(user);
	}
	private Integer countUsers() {
        Query query = entityManager.createQuery("SELECT COUNT(p.id) FROM User p");
        return ((Long) query.getSingleResult()).intValue();
    }
	
	public User findUserByEmail(String email) {
		Query requete = entityManager.createQuery(JPQL_SELECT_PAR_EMAIL);
		requete.setParameter(PARAM_EMAIL, email);
		User user = (User) requete.getSingleResult();
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllTheUsers(){
		return entityManager.createNativeQuery("select * from User", User.class)
                .getResultList();
	}
	
    @GET
    @Path("{id}")
    public User getUser( @PathParam("id") Long id) {
        return entityManager.find(User.class, id);
    }
 
    @POST
    public User saveUser(User user) {
        if (user.getId()==0) {
            User userToSave = new User();
            userToSave.setEmail(user.getEmail());
            userToSave.setPassword(user.getPassword());
            userToSave.setFirstName(user.getFirstName());
            userToSave.setLastName(user.getLastName());
            userToSave.setAdress(user.getAdress());
            userToSave.setPhoneNumber(user.getPhoneNumber());
            userToSave.setBirthday(user.getBirthday());
            entityManager.persist(user);
        } else {
            User userToUpdate = getUser(user.getId());
            userToUpdate.setEmail(user.getEmail());
            userToUpdate.setPassword(user.getPassword());
            userToUpdate.setFirstName(user.getFirstName());
            userToUpdate.setLastName(user.getLastName());
            userToUpdate.setAdress(user.getAdress());
            userToUpdate.setPhoneNumber(user.getPhoneNumber());
            userToUpdate.setBirthday(user.getBirthday());
            user = entityManager.merge(userToUpdate);
        }
 
        return user;
    }
 
    @DELETE
    @Path("{id}")
    public void deleteUser(@PathParam("id") Long id) {
        entityManager.remove(getUser(id));
    }
    @SuppressWarnings("unchecked")
    private List<User> findUsers(int startPosition, int maxResults, String sortFields, String sortDirections) {
        Query query = entityManager.createQuery("SELECT p FROM User p ORDER BY " + sortFields + " " + sortDirections);
        query.setFirstResult(startPosition);
        query.setMaxResults(maxResults);
        return query.getResultList();
    }
    public PaginatedListWrapper<User> findUsers(PaginatedListWrapper<User> wrapper) {
        wrapper.setTotalResults(countUsers());
        int start = (wrapper.getCurrentPage() - 1) * wrapper.getPageSize();
        wrapper.setList(findUsers(start,
                                    wrapper.getPageSize(),
                                    wrapper.getSortFields(),
                                    wrapper.getSortDirections()));
        return wrapper;
    }
 
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public PaginatedListWrapper<User> listUsers(@DefaultValue("1")
                                                    @QueryParam("page")
                                                    Integer page,
                                                    @DefaultValue("id")
                                                    @QueryParam("sortFields")
                                                    String sortFields,
                                                    @DefaultValue("asc")
                                                    @QueryParam("sortDirections")
                                                    String sortDirections) {
        PaginatedListWrapper<User> paginatedListWrapper = new PaginatedListWrapper<>();
        paginatedListWrapper.setCurrentPage(page);
        paginatedListWrapper.setSortFields(sortFields);
        paginatedListWrapper.setSortDirections(sortDirections);
        paginatedListWrapper.setPageSize(5);
        return findUsers(paginatedListWrapper);
    }
}